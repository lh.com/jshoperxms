package com.jshoperxms.action.mall.backstage.feedback;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.service.PostsReplyTService;
import com.jshoperxms.service.PostsTService;
import com.jshoperxms.service.PostsZanRecordTService;

@Namespace("/mall/feedback/posts")
@ParentPackage("jshoperxms")
public class PostsTAction extends BaseTAction {

	@Resource
	private PostsTService postsTService;


}
