package com.jshoperxms.action.mall.backstage.feedback;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.service.PostsReplyTService;

@Namespace("/mall/feedback/postsreply")
@ParentPackage("jshoperxms")
public class PostsReplyTAction extends BaseTAction {

	@Resource
	private PostsReplyTService postsReplyTService;
	
}
