define([ 
         'angular',
         'angular-route',
         'angular-resource',
         'jquery',
         'bootstrap3',
         './login/index',
         './index/index',
         './goodstype/index',
         './goodsattribute/index',
         './feedback/index'], function(angular) {
	'use strict';
	return angular.module('app', [ 'loginmodule', 'indexmodule',
			'goodstypemodule','goodsattributemodule','feedbackmodule', 'ngRoute','ngResource']);
});