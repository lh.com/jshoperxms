/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(['./app'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/index', {
            templateUrl: 'app/index/indexmain.html',
            controller: 'index'
        });
        /**
         * 商品类型管理路由
         */
        $routeProvider.when('/goodstypement',{
        	templateUrl:'app/goods/goodstypement.html',
        	controller:'goodstypelist'
        });
        $routeProvider.when('/goodstype',{
        	templateUrl:'app/goods/goodstype.html',
        	controller:'goodstype'
        });
        
        /**
         * 用户反馈管理
         */
        $routeProvider.when('/feedbackment',{
        	templateUrl:'app/feedback/feedbackment.html',
        	controller:'feedbacklist'
        });
        $routeProvider.when('/feedback',{
        	templateUrl:'app/feedback/feedback.html',
        	controller:'feedback'
        });

        /**
         * 商品属性管理路由
         */
        $routeProvider.when('/goodsattributement',{
            templateUrl:'app/goods/goodsattributement.html',
            controller:'goodsattributelist'
        });
        $routeProvider.when('/goodsattribute',{
            templateUrl:'app/goods/goodsattribute.html',
            controller:'goodsattribute'
        });

        /**
         * 商品品牌类型路由管理
         */
        $routeProvider.when('/goodstypebrandment',{
            templateUrl:'app/goods/goodstypebrandment.html',
            controller:'goodstypebrandlist'
        });
        $routeProvider.when('/goodstypebrand',{
            templateUrl:'app/goods/goodstypebrand.html',
            controller:'goodstypebrand'
        });
        
    }]);
});
