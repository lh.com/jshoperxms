package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GlobalParamM;
import com.jshoperxms.service.GlobalParamService;


@Service("globalParamService")
@Scope("prototype")
public class GlobalParamServiceImpl extends BaseTServiceImpl<GlobalParamM>
		implements GlobalParamService {
	
}
