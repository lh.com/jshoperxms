package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsTwocodeRpT;
import com.jshoperxms.service.GoodsTwocodeRelationshipTService;
@Service("goodsTwocodeRelationshipTService")
@Scope("prototype")
public class GoodsTwocodeRelationshipTServiceImpl extends BaseTServiceImpl<GoodsTwocodeRpT>implements
		GoodsTwocodeRelationshipTService {


}
