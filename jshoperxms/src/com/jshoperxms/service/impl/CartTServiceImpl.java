package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.CartT;
import com.jshoperxms.service.CartTService;

@Service("cartTService")
@Scope("prototype")
public class CartTServiceImpl extends BaseTServiceImpl<CartT> implements CartTService {

}
