package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.GoodsCategoryT;
import com.jshoperxms.service.GoodsCategoryTService;

@Service("goodsCategoryTService")
@Scope("prototype")
public class GoodsCategoryTServiceImpl extends BaseTServiceImpl<GoodsCategoryT> implements GoodsCategoryTService {


}
