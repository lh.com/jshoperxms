package com.jshoperxms.action.mall.backstage.vo;

import java.io.Serializable;

/**
 * 商品类型品牌
* @ClassName: GoodsTypeBrandsVo 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author jcchen
* @date 2015年10月14日 下午4:28:41 
*
 */
public class GoodsTypeBrandsVo implements Serializable {

	private String id;
	private String brandname;
	private int sort;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBrandname() {
		return brandname;
	}
	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	
	
}
