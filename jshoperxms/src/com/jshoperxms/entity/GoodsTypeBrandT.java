package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the goods_type_brand_t database table.
 * 
 */
@Entity
@Table(name="goods_type_brand_t")
@NamedQuery(name="GoodsTypeBrandT.findAll", query="SELECT g FROM GoodsTypeBrandT g")
public class GoodsTypeBrandT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="GOODS_TYPE_BRAND_TID")
	private String goodsTypeBrandTid;

	private String brandid;

	private String brandname;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	private String creatorid;

	@Column(name="GOODS_TYPE_ID")
	private String goodsTypeId;

	@Column(name="GOODS_TYPE_NAME")
	private String goodsTypeName;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private int versiont;

	
	private String brandlist;
	
	private int sort;
	
	
	public GoodsTypeBrandT() {
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getBrandlist() {
		return brandlist;
	}

	public void setBrandlist(String brandlist) {
		this.brandlist = brandlist;
	}

	public String getGoodsTypeBrandTid() {
		return this.goodsTypeBrandTid;
	}

	public void setGoodsTypeBrandTid(String goodsTypeBrandTid) {
		this.goodsTypeBrandTid = goodsTypeBrandTid;
	}

	public String getBrandid() {
		return this.brandid;
	}

	public void setBrandid(String brandid) {
		this.brandid = brandid;
	}

	public String getBrandname() {
		return this.brandname;
	}

	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getCreatorid() {
		return this.creatorid;
	}

	public void setCreatorid(String creatorid) {
		this.creatorid = creatorid;
	}

	public String getGoodsTypeId() {
		return this.goodsTypeId;
	}

	public void setGoodsTypeId(String goodsTypeId) {
		this.goodsTypeId = goodsTypeId;
	}

	public String getGoodsTypeName() {
		return this.goodsTypeName;
	}

	public void setGoodsTypeName(String goodsTypeName) {
		this.goodsTypeName = goodsTypeName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public int getVersiont() {
		return this.versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

}