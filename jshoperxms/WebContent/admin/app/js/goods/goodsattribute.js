var mModule=angular.module("goodsattributenModule",[]);

/**
 * 货物规格路由控制
 */
mModule.config(function($routeProvider){
	$routeProvider
	.when('/goodsattributement',{
		templateUrl:'../admin/app/goods/goodsattributement.html',
		controller:'goodsattribute'
	});
});

mModule.controller('goodsattribute',['$scope','$http',function($scope,$http){
	$scope.title="商品属性列表";
}]);

