package com.jshoperxms.action.mall.backstage.feedback;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.service.PostsZanRecordTService;

@Namespace("/mall/feedback/postszanrecord")
@ParentPackage("jshoperxms")
public class PostsZanRecordTAction extends BaseTAction {

	@Resource
	private PostsZanRecordTService postsZanRecordTService;
}
