package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the expresstemplete_t database table.
 * 
 */
@Entity
@Table(name="expresstemplete_t")
@NamedQuery(name="ExpresstempleteT.findAll", query="SELECT e FROM ExpresstempleteT e")
public class ExpresstempleteT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String expresstempleteid;

	private String day;

	@Column(name="EXPRESS_CSS")
	private String expressCss;

	@Column(name="EXPRESS_IMG")
	private String expressImg;

	private String hour;

	@Lob
	@Column(name="KINDEDITOR_CODE")
	private String kindeditorCode;

	private String logisticsid;

	private String minutes;

	private String month;

	private String notes;

	private String orderid;

	private String quantity;

	@Column(name="RECIPIENT_CITY")
	private String recipientCity;

	@Column(name="RECIPIENT_CONTACTOR")
	private String recipientContactor;

	@Column(name="RECIPIENT_COUNTRY")
	private String recipientCountry;

	@Column(name="RECIPIENT_DISTRICT")
	private String recipientDistrict;

	@Column(name="RECIPIENT_MOBILE")
	private String recipientMobile;

	@Column(name="RECIPIENT_NAME")
	private String recipientName;

	@Column(name="RECIPIENT_POSTCODE")
	private String recipientPostcode;

	@Column(name="RECIPIENT_PROVINCE")
	private String recipientProvince;

	@Column(name="RECIPIENT_STREET")
	private String recipientStreet;

	@Column(name="RECIPIENT_TELNO")
	private String recipientTelno;

	@Column(name="RIGHT_TAG")
	private String rightTag;

	@Column(name="SEND_CITY")
	private String sendCity;

	@Column(name="SEND_CONTACTOR")
	private String sendContactor;

	@Column(name="SEND_COUNTRY")
	private String sendCountry;

	@Column(name="SEND_DISTRICT")
	private String sendDistrict;

	@Column(name="SEND_MOBILE")
	private String sendMobile;

	@Column(name="SEND_NAME")
	private String sendName;

	@Column(name="SEND_PROVINCE")
	private String sendProvince;

	@Column(name="SEND_STREET")
	private String sendStreet;

	@Column(name="SEND_TELNO")
	private String sendTelno;

	@Column(name="SEND_TIME")
	private String sendTime;

	private String state;

	private String year;

	public ExpresstempleteT() {
	}

	public String getExpresstempleteid() {
		return this.expresstempleteid;
	}

	public void setExpresstempleteid(String expresstempleteid) {
		this.expresstempleteid = expresstempleteid;
	}

	public String getDay() {
		return this.day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getExpressCss() {
		return this.expressCss;
	}

	public void setExpressCss(String expressCss) {
		this.expressCss = expressCss;
	}

	public String getExpressImg() {
		return this.expressImg;
	}

	public void setExpressImg(String expressImg) {
		this.expressImg = expressImg;
	}

	public String getHour() {
		return this.hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getKindeditorCode() {
		return this.kindeditorCode;
	}

	public void setKindeditorCode(String kindeditorCode) {
		this.kindeditorCode = kindeditorCode;
	}

	public String getLogisticsid() {
		return this.logisticsid;
	}

	public void setLogisticsid(String logisticsid) {
		this.logisticsid = logisticsid;
	}

	public String getMinutes() {
		return this.minutes;
	}

	public void setMinutes(String minutes) {
		this.minutes = minutes;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getOrderid() {
		return this.orderid;
	}

	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}

	public String getQuantity() {
		return this.quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getRecipientCity() {
		return this.recipientCity;
	}

	public void setRecipientCity(String recipientCity) {
		this.recipientCity = recipientCity;
	}

	public String getRecipientContactor() {
		return this.recipientContactor;
	}

	public void setRecipientContactor(String recipientContactor) {
		this.recipientContactor = recipientContactor;
	}

	public String getRecipientCountry() {
		return this.recipientCountry;
	}

	public void setRecipientCountry(String recipientCountry) {
		this.recipientCountry = recipientCountry;
	}

	public String getRecipientDistrict() {
		return this.recipientDistrict;
	}

	public void setRecipientDistrict(String recipientDistrict) {
		this.recipientDistrict = recipientDistrict;
	}

	public String getRecipientMobile() {
		return this.recipientMobile;
	}

	public void setRecipientMobile(String recipientMobile) {
		this.recipientMobile = recipientMobile;
	}

	public String getRecipientName() {
		return this.recipientName;
	}

	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}

	public String getRecipientPostcode() {
		return this.recipientPostcode;
	}

	public void setRecipientPostcode(String recipientPostcode) {
		this.recipientPostcode = recipientPostcode;
	}

	public String getRecipientProvince() {
		return this.recipientProvince;
	}

	public void setRecipientProvince(String recipientProvince) {
		this.recipientProvince = recipientProvince;
	}

	public String getRecipientStreet() {
		return this.recipientStreet;
	}

	public void setRecipientStreet(String recipientStreet) {
		this.recipientStreet = recipientStreet;
	}

	public String getRecipientTelno() {
		return this.recipientTelno;
	}

	public void setRecipientTelno(String recipientTelno) {
		this.recipientTelno = recipientTelno;
	}

	public String getRightTag() {
		return this.rightTag;
	}

	public void setRightTag(String rightTag) {
		this.rightTag = rightTag;
	}

	public String getSendCity() {
		return this.sendCity;
	}

	public void setSendCity(String sendCity) {
		this.sendCity = sendCity;
	}

	public String getSendContactor() {
		return this.sendContactor;
	}

	public void setSendContactor(String sendContactor) {
		this.sendContactor = sendContactor;
	}

	public String getSendCountry() {
		return this.sendCountry;
	}

	public void setSendCountry(String sendCountry) {
		this.sendCountry = sendCountry;
	}

	public String getSendDistrict() {
		return this.sendDistrict;
	}

	public void setSendDistrict(String sendDistrict) {
		this.sendDistrict = sendDistrict;
	}

	public String getSendMobile() {
		return this.sendMobile;
	}

	public void setSendMobile(String sendMobile) {
		this.sendMobile = sendMobile;
	}

	public String getSendName() {
		return this.sendName;
	}

	public void setSendName(String sendName) {
		this.sendName = sendName;
	}

	public String getSendProvince() {
		return this.sendProvince;
	}

	public void setSendProvince(String sendProvince) {
		this.sendProvince = sendProvince;
	}

	public String getSendStreet() {
		return this.sendStreet;
	}

	public void setSendStreet(String sendStreet) {
		this.sendStreet = sendStreet;
	}

	public String getSendTelno() {
		return this.sendTelno;
	}

	public void setSendTelno(String sendTelno) {
		this.sendTelno = sendTelno;
	}

	public String getSendTime() {
		return this.sendTime;
	}

	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

}